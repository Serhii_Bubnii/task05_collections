package com.bubnii.view;

import com.bubnii.controller.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import static com.bubnii.model.ClassName.getCurrentClassName;

public class ViewConsoleMap {

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private ControllerІmpl controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner scanner;

    public ViewConsoleMap() {
        this.controller = new ControllerІmpl();
        this.scanner = new Scanner(System.in);
        this.menu = new LinkedHashMap<>();
        menu.put("PUT", "  PUT. \t\tPut item in binary tree map");
        menu.put("GET", "  GET. \t\tReturns the value corresponding to the key");
        menu.put("REMOVE", "  REMOVE. \tRemoves the element with the key key");
        menu.put("SIZE", "  SIZE. \t\tReturns the number of items");
        menu.put("CLEAN", "  CLEAN. \t\tCleaning the tree");
        menu.put("PRINT", "  PRINT. \t\tPrint tree map");
        menu.put("ISEMPTY", "  ISEMPTY. \tChecks if the tree is empty");
        menu.put("CONKEY", "  CONKEY. \tChecks if there is such an item with that key");
        menu.put("CONVALUE", "  CONVALUE. \tChecks if there is such an item with this value");
        menu.put("VALUES", "  VALUES. \tReturns the values of all tree elements as a collection");
        menu.put("KEYSET", "  KEYSET. \tReturns the map view as a set of all keys");
        menu.put("ENTRYSET", "  ENTRYSET. \tReturns a key-value set");
        menu.put("QUIT", "  QUIT. \t\tQuit Application.");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("PUT", this::putInTree);
        methodsMenu.put("GET", this::getOfTree);
        methodsMenu.put("REMOVE", this::removeOfTree);
        methodsMenu.put("SIZE", this::sizeBinaryTree);
        methodsMenu.put("CLEAN", this::clearBinaryTree);
        methodsMenu.put("PRINT", this::printBinaryTree);
        methodsMenu.put("ISEMPTY", this::isEmptyBinaryTree);
        methodsMenu.put("CONKEY", this::containsKeyOfTree);
        methodsMenu.put("CONVALUE", this::containsValueOfTree);
        methodsMenu.put("VALUES", this::valuesBinaryTree);
        methodsMenu.put("KEYSET", this::keySetBinaryTree);
        methodsMenu.put("ENTRYSET", this::entrySetBinaryTree);
    }

    private void putInTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Please, enter value:");
        String value = scanner.next();
        logger.info("Put Item " + controller.putItem(key, value));
    }

    private void getOfTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Get item " + controller.getItem(key));
    }

    private void removeOfTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Remove item " + controller.removeItem(key));
    }

    private void containsKeyOfTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Contains Key " + controller.containsKeyBinaryTreeMap(key));
    }

    private void containsValueOfTree() {
        logger.info("Please, enter value:");
        String value = scanner.next();
        logger.info("Contains Value " + controller.containsValueBinaryTreeMap(value));
    }

    private void sizeBinaryTree() {
        logger.info("Size tree " + controller.sizeBinaryTreeMap());
    }

    private void clearBinaryTree() {
        controller.clearBinaryTreeMap();
    }

    private void printBinaryTree() {
        logger.info(controller.printBinaryTreeMap());
    }

    private void isEmptyBinaryTree() {
        logger.info("Is the tree is empty? " + controller.isEmptyBinaryTreeMap());
    }

    private void valuesBinaryTree() {
        logger.info("Values of all tree elements as a collection " + controller.valuesBinaryTreeMap());
    }

    private void keySetBinaryTree() {
        logger.info("Keys of all tree elements as a collection " + controller.keySetBinaryTreeMap());
    }

    private void entrySetBinaryTree() {
        logger.info("Key-value set " + controller.entrySetBinaryTreeMap());
    }

    private void outputMenu() {
        logger.info("MENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        while (true) {
            try {
                outputMenu();
                logger.info("\nPlease, select menu point.");
                keyMenu = scanner.next().toUpperCase();

                if (keyMenu.equals("QUIT")) {
                    System.exit(0);
                }
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info("Selection out of range. Try again");
            }
        }
    }
}
