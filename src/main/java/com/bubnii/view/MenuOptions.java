package com.bubnii.view;

public enum MenuOptions {
    ISEMPTY("Checks if the array is empty"),
    PUT("Put item"),
    REMOVE("Remove item by key"),
    GET("Get item by key"),
    SIZE("Returns the number of items"),
    CONKEY("Checks if there is such an item with that key"),
    CONVALUE("Checks if there is such an item with this value"),
    CLEAN("Cleaning the tree"),
    KEYSET("Returns the map view as a set of all keys"),
    VALUES("Returns the values of all tree elements as a collection"),
    PRINT("Print tree map"),
    ENTRYSET("Returns a key-value set"),
    QUIT("Quit");

    private String meaning;

    MenuOptions(String meaning) {
        this.meaning = meaning;
    }

    public String getMeaning() {
        return meaning;
    }
}
