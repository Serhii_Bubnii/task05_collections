package com.bubnii.view;

import com.bubnii.controller.ControllerІmpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

import static com.bubnii.model.ClassName.getCurrentClassName;

public class ViewConsoleEnum {

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private Scanner scanner = new Scanner(System.in);
    private ControllerІmpl controller = new ControllerІmpl();

    public void show() {
        MenuOptions options;

        while (true) {
            try {
                outputMenu();

                options = MenuOptions.valueOf(scanner.next().toUpperCase());

                switch (options) {
                    case PUT:
                        putInTree();
                        break;
                    case GET:
                        getOfTree();
                        break;
                    case REMOVE:
                        removeOfTree();
                        break;
                    case SIZE:
                        logger.info("Size tree " + controller.sizeBinaryTreeMap());
                        break;
                    case CLEAN:
                        controller.clearBinaryTreeMap();
                        break;
                    case PRINT:
                        logger.info(controller.printBinaryTreeMap());
                        break;
                    case ISEMPTY:
                        logger.info("Is the tree is empty? " + controller.isEmptyBinaryTreeMap());
                        break;
                    case CONKEY:
                        containsKeyOfTree();
                        break;
                    case CONVALUE:
                        containsValueOfTree();
                        break;
                    case VALUES:
                        logger.info("Values of all tree elements as a collection " + controller.valuesBinaryTreeMap());
                        break;
                    case KEYSET:
                        logger.info("Keys of all tree elements as a collection " + controller.keySetBinaryTreeMap());
                        break;
                    case ENTRYSET:
                        logger.info("Key-value set " + controller.entrySetBinaryTreeMap());
                        break;
                    case QUIT:
                        logger.info("Goodbye.");
                        System.exit(0);
                    default:
                        logger.info("Selection out of range. Try again");
                }
            } catch (IllegalArgumentException e) {
                logger.info("Selection out of range. Try again");
            }
        }
    }

    void putInTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Please, enter value:");
        String value = scanner.next();
        logger.info("Put Item " + controller.putItem(key, value));
    }

    void getOfTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Get item " + controller.getItem(key));
    }

    void removeOfTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Remove item " + controller.removeItem(key));
    }

    void containsKeyOfTree() {
        logger.info("Please, enter key:");
        int key = scanner.nextInt();
        logger.info("Contains Key " + controller.containsKeyBinaryTreeMap(key));
    }

    void containsValueOfTree() {
        logger.info("Please, enter value:");
        String value = scanner.next();
        logger.info("Contains Value " + controller.containsValueBinaryTreeMap(value));
    }

    private void outputMenu() {
        logger.info("\tEnter:");
        logger.info("\tPUT. \t\tPut item in binary tree map");
        logger.info("\tGET. \t\tReturns the value corresponding to the key");
        logger.info("\tREMOVE. \tRemoves the element with the key key, returning the value of this element (if any) and null");
        logger.info("\tSIZE. \t\tReturns the number of items");
        logger.info("\tCLEAN. \t\tCleaning the tree");
        logger.info("\tPRINT. \t\tPrint tree map");
        logger.info("\tISEMPTY. \tChecks if the tree is empty");
        logger.info("\tCONKEY. \tChecks if there is such an item with that key");
        logger.info("\tCONVALUE. \tChecks if there is such an item with this value");
        logger.info("\tVALUES. \tReturns the values of all tree elements as a collection");
        logger.info("\tKEYSET. \tReturns the map view as a set of all keys");
        logger.info("\tENTRYSET. \tReturns a key-value set");
        logger.info("\tQUIT. \t\tQuit Application.");
        logger.info("\n\tSelection -> ");
    }
}
