package com.bubnii.controller;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface Controller {

    String putItem(int key, String values);

    String getItem(int key);

    String removeItem(int key);

    int sizeBinaryTreeMap();

    String printBinaryTreeMap();

    boolean isEmptyBinaryTreeMap();

    boolean containsKeyBinaryTreeMap(int key);

    boolean containsValueBinaryTreeMap(String value);

    void clearBinaryTreeMap();

    Collection<String> valuesBinaryTreeMap();

    Set<Integer> keySetBinaryTreeMap();

    Set<Map.Entry<Integer, String>> entrySetBinaryTreeMap();
}
