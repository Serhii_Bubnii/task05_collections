package com.bubnii.controller;

import com.bubnii.model.BinaryTreeMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ControllerІmpl implements Controller {

    private BinaryTreeMap<Integer, String> binaryTreeMap;

    public ControllerІmpl() {
        this.binaryTreeMap = new BinaryTreeMap<>();
    }

    public BinaryTreeMap<Integer, String> getBinaryTreeMap() {
        return binaryTreeMap;
    }

    @Override
    public String putItem(int key, String values) {
        return binaryTreeMap.put(key, values);
    }

    @Override
    public String getItem(int key) {
        return binaryTreeMap.get(key);
    }

    @Override
    public String removeItem(int key) {
        return binaryTreeMap.remove(key);
    }

    @Override
    public int sizeBinaryTreeMap() {
        return binaryTreeMap.size();
    }

    @Override
    public String printBinaryTreeMap() {
        return binaryTreeMap.toString();
    }

    @Override
    public boolean isEmptyBinaryTreeMap() {
        return binaryTreeMap.isEmpty();
    }

    @Override
    public boolean containsKeyBinaryTreeMap(int key) {
        return binaryTreeMap.containsKey(key);
    }

    @Override
    public boolean containsValueBinaryTreeMap(String value) {
        return binaryTreeMap.containsKey(value);
    }

    @Override
    public void clearBinaryTreeMap() {
        binaryTreeMap.clear();
    }

    @Override
    public Collection<String> valuesBinaryTreeMap() {
        return binaryTreeMap.values();
    }

    @Override
    public Set<Integer> keySetBinaryTreeMap() {
        return binaryTreeMap.keySet();
    }

    @Override
    public Set<Map.Entry<Integer, String>> entrySetBinaryTreeMap() {
        return binaryTreeMap.entrySet();
    }
}
